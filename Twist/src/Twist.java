import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Twist {

	public static void main(String[] args) {
		
		//String text = eingabe();
		String text = "Sicherheitsnadel";
		int[] key = key(text.toCharArray().length);
		String twisted = twist(text, key);
		System.out.println(twisted);
		String untwisted = untwist(twisted, key);
		System.out.println(untwisted);
	}
	
	// Eingabe
	public static String eingabe() {
		Scanner sc = new Scanner(System.in);
		System.out.println();
		return sc.next();
	}
	
	// generate key
	public static int[] key(int length) {
		int[] key = new int[(length - 2)];
		// If running on Java 6 or older, use `new Random()` on RHS here
	    Random rnd = ThreadLocalRandom.current();
	    for (int i = length - 2; i > 0; i--) {
	      int index = rnd.nextInt(i) + 1;      
	      key[i - 1] = index;
	    }
		return key;
	}
	
	// verschlüsseln
	public static String twist(String text, int[] key) {
		char charArr[] = text.toCharArray();

	    for (int i = 0; i < key.length; i++) {
	      // Simple swap
	      char a = charArr[key[i]];
	      charArr[key[i]] = charArr[i+1];
	      charArr[i+1] = a;
	    }
		return new String(charArr);
	}
	
	// entschlüsseln
	public static String untwist(String text, int[] key) {
		char charArr[] = text.toCharArray();
		// swap back
		for (int i = key.length - 1; i > 0; i--) {
	    	char a = charArr[i+1];
	    	charArr[i+1] = charArr[key[i]];
	    	charArr[key[i]] = a;
		}
		return new String(charArr);
	}

	// shuffle array
	public static void shuffleArray(char[] ar, int[] key) {
		//int[] key = new int[ar.length - 2];
		// If running on Java 6 or older, use `new Random()` on RHS here
	    Random rnd = ThreadLocalRandom.current();
	    for (int i = ar.length - 2; i > 1; i--) {
	      int index = rnd.nextInt(i) + 1;
	      key[i-1] = index;
	      // Simple swap
	      char a = ar[index];
	      ar[index] = ar[i];
	      ar[i] = a;
	    }
	}
}
