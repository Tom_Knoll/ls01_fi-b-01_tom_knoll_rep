import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Scanner;

class Fahrkartenautomat {
	
    public static void main(String[] args) {
    	
    	while (true) {
    		double zuZahlenderBetrag = fahrkartenbestellungErfassen();
    		double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
    		fahrkartenAusgeben();
    		rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
    	}
    }
    
    public static double fahrkartenbestellungErfassen() {
    	int typ;
    	int anzahlTickets;
    	Scanner tastatur = new Scanner(System.in);
    	double zuZahlenderBetrag = 0;
    	double ticketPreis = 0;
    	boolean moechteBezahlen = false;
    	String[] bezeichnung = {"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", 
    	                        "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC", 
    	                        "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC"};
    	double[] preise = {12.9, 3.3, 3.6, 1.9, 8.6, 9.0, 9.6, 23.5, 24.3, 24.9};
    	
    	System.out.println("Fahrkartenbestellvorgang:\n"
    			+ "========================= \n\n");
    	System.out.printf("%-14s%-35s%s%n", "Auswahlnummer", "Bezeichnung", "Preis in Euro");
    	System.out.printf("%-2d:%11s%-35s%n", 0, "", "Bezahlen");
    	for (int i = 0; i < bezeichnung.length; i++) {
    		System.out.printf("%-2d:%11s%-35s%5.2f �%n", i+1, "",bezeichnung[i], preise[i]);
    	}
    	
    	// Auswahl mehrerer Tickets
    	while (!moechteBezahlen) {
    		//anzahlTickets = 0;
    		typ = 0;
    		
    		// Ticketauswahl
    		System.out.print("\nIhre Wahl: ");
    		typ = tastatur.nextInt();
    		while (typ < 0 || typ > 10) {
    			System.out.println(">>falsche Eingabe<<");
        		System.out.print("\nIhre Wahl: ");
        		typ = tastatur.nextInt();		
        	}
        	
    		if (typ == 0) {
    			moechteBezahlen = true;
    		} else {
    			ticketPreis = preise[typ-1];
    		}
            
            // Anzahl der Tickets
        	if (!moechteBezahlen) {
        		System.out.print("\nAnzahl der Tickets (1-10): ");
        		anzahlTickets = tastatur.nextInt();
        		while (anzahlTickets < 1 || anzahlTickets > 10) {
        			System.out.print("\nBitte geben Sie eine Zahl von 1 bis 10 ein");
        			System.out.print("\nAnzahl der Tickets (1-10): ");
                	anzahlTickets = tastatur.nextInt();
        		} 
                zuZahlenderBetrag += ticketPreis * anzahlTickets;
        	}   
    	}
    	return zuZahlenderBetrag;
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	// Geldeinwurf
        // -----------
    	Scanner tastatur = new Scanner(System.in);
        double eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("Noch zu zahlen: %1.2f EURO\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
       	   double eingeworfeneM�nze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneM�nze;
        }
    	return eingezahlterGesamtbetrag;
    }
    
    public static void fahrkartenAusgeben() {
    	// Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
 			warte(250);
        }
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
    	// R�ckgeldberechnung und -Ausgabe
        // -------------------------------
        double r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if(r�ckgabebetrag > 0.0)
        {
     	   System.out.printf("Der R�ckgabebetrag in H�he von %1.2f EURO ", r�ckgabebetrag);
     	   System.out.println("wird in folgenden M�nzen ausgezahlt:");
     	   
            while(r�ckgabebetrag >= 2.0) { // 2 EURO-M�nzen  
            	muenzeAusgeben(2, "EURO");
 	          	r�ckgabebetrag -= 2.0;
            }
            
            while(r�ckgabebetrag >= 1.0) { // 1 EURO-M�nzen            
            	muenzeAusgeben(1, "EURO");
            	r�ckgabebetrag -= 1;
            }
            
            while(r�ckgabebetrag >= 0.5) { // 50 CENT-M�nzen       
            	muenzeAusgeben(50, "CENT");
            	r�ckgabebetrag -= 0.5;
            }
            
            while(r�ckgabebetrag >= 0.2) { // 20 CENT-M�nzen           
            	muenzeAusgeben(20, "CENT");
  	          	r�ckgabebetrag -= 0.2;
  	          	r�ckgabebetrag = new BigDecimal(r�ckgabebetrag).setScale(2, RoundingMode.HALF_UP).doubleValue();
            }
            
            while(r�ckgabebetrag >= 0.1) { // 10 CENT-M�nzen           
            	muenzeAusgeben(10, "CENT");
            	r�ckgabebetrag -= 0.1;
            	r�ckgabebetrag = new BigDecimal(r�ckgabebetrag).setScale(2, RoundingMode.HALF_UP).doubleValue();
            }
            
            while(r�ckgabebetrag >= 0.05) { // 5 CENT-M�nzen           
            	muenzeAusgeben(5, "CENT");
  	          	r�ckgabebetrag -= 0.05;
  	          	r�ckgabebetrag = new BigDecimal(r�ckgabebetrag).setScale(2, RoundingMode.HALF_UP).doubleValue();
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir w�nschen Ihnen eine gute Fahrt. \n\n");
    }
    
    public static void warte(int millisekunde) {
    	try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public static void muenzeAusgeben(int betrag, String einheit) {
    	System.out.println(betrag + " " + einheit);
    }
}