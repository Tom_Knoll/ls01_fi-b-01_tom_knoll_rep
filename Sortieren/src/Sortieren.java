import java.util.Scanner;

public class Sortieren {

	public static void main(String[] args) {
		System.out.println("Gib 3 Zeichen ein, Zahlen oder Buchstaben");
		Scanner sc = new Scanner(System.in);
		char a = sc.next().charAt(0);
		char b = sc.next().charAt(0);
		char c = sc.next().charAt(0);
		
		int valueA = String.valueOf(a).hashCode();
		int valueB = String.valueOf(b).hashCode();
		int valueC = String.valueOf(c).hashCode();
		
		if (valueA < valueB) { // a < b
			if (valueB < valueC) { // a < b < c	
				System.out.println(a + "\n" + b + "\n" + c);
			} else if (valueC < valueA) { // c < a < b
				System.out.println(c + "\n" + a + "\n" + b);
			} else { // a < c < b
				System.out.println(a + "\n" + c + "\n" + b);
			}
		} else if (valueB < valueA) { // b < a
			if (valueA < valueC) { // b < a < c
				System.out.println(b + "\n" + a + "\n" + c);
			} else if (valueC < valueB) { // c < b < a
				System.out.println(c + "\n" + b + "\n" + a);
			} else { // b < c < a
				System.out.println(b + "\n" + c + "\n" + a);
			}
		} else {
			System.out.println("?");
		}
	}
}
