import java.util.Scanner;

public class PCHaendler {
	
	public static void main(String[] args) {
		
		String artikel = liesString("Was m�chten Sie bestellen?");
		int anzahl = liesInt("Geben Sie die Anzahl ein:");
		double preis = liesDouble("Geben Sie den Nettopreis ein:");
		double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double netto = berechneGesamtnettopreis(anzahl, preis);
		double brutto = berechneGesamtbruttopreis(netto, mwst);
		rechnungAusgeben(artikel, anzahl, netto, brutto, mwst);
		
	}
	
	public static String liesString(String ausgabe) {
		// Benutzereingaben lesen
		Scanner myScanner = new Scanner(System.in);
		System.out.println(ausgabe);
		return myScanner.nextLine();
	}
	
	public static int liesInt(String ausgabe) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(ausgabe);
		return myScanner.nextInt();
	}
	
	public static double liesDouble(String ausgabe) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(ausgabe);
		return myScanner.nextDouble();
	}
	
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		return anzahl * nettopreis;
	}
	
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		return nettogesamtpreis * (1 + mwst / 100);
	}
	
	public static void rechnungAusgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}
}
