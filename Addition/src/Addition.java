import java.util.Scanner;

class Addition {

    public static void main(String[] args) {
        addition();
    }
    
    public static void addition() {
    	programmhinweis();
    	double zahl1 = eingabe("1. Zahl:");
    	double zahl2 = eingabe("2. Zahl:");
    	double erg = verarbeitung(zahl1, zahl2);
    	ausgabe(erg, zahl1, zahl2);
    }
    
    public static void programmhinweis() {
    	//1.Programmhinweis
        System.out.println("Hinweis: ");
        System.out.println("Das Programm addiert 2 eingegebene Zahlen. ");
    }
    
    public static void ausgabe(double erg, double zahl1, double zahl2) {
    	 //2.Ausgabe
        System.out.println("Ergebnis der Addition");
        System.out.printf("%.2f = %.2f + %.2f", erg, zahl1, zahl2);
    }
    
    public static double eingabe(String ausgabe) {
    	Scanner sc = new Scanner(System.in);
    	 //4.Eingabe
    	System.out.println(ausgabe);
        return sc.nextDouble();
    }
    public static double verarbeitung(double zahl1, double zahl2) {
    	//3.Verarbeitung
        return zahl1 + zahl2;
    }
    
}